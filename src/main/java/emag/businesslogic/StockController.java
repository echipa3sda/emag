package facebook.businesslogic;

import facebook.model.Item;
import facebook.model.SearchResult;

import java.util.List;

class StockController {
    private final Stock stock;

    StockController(Stock stock) {
        this.stock = stock;
    }

    <T extends Item> List<SearchResult<T>> search(String itemName, Class<T> itemType) {
        return stock.search(itemName, itemType);
    }
}
