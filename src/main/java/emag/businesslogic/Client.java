package facebook.businesslogic;

import facebook.model.Item;
import facebook.model.SearchResult;
import facebook.shared.OutOfStockException;

import java.util.List;

public class Client implements Runnable {
    private static final int MILLISECONDS_TO_SECONDS_RATIO = 1000;
    private final CartController cartController;
    private final StockController stockController;
    private final Item itemToBuy;

    public Client(Stock stock, Item itemToBuy) {
        this(new CartController(stock, new Cart()),
                new StockController(stock), itemToBuy);
    }

    // TODO: implement list in stock and simulate taking the first item
    // instead of using a passed item via the constructor
    private Client(CartController cartController, StockController stockController,
                  Item itemToBuy) {
        this.stockController = stockController;
        this.cartController = cartController;
        this.itemToBuy = itemToBuy;
    }

    public int userAdd(Item item, int quantity) {
        try {
            return cartController.userAdd(item, quantity);
        } catch (OutOfStockException e) {
            //System.out.println(e.getMessage());
        }
        return 0;
    }

    public int userRemove(Item item, int quantity) {
        return cartController.userRemove(item, quantity);
    }

    public void userRemoveAll() {
        cartController.userRemoveAll();
    }

    public <T extends Item> List<SearchResult<T>> search(String itemName, Class<T> itemType) {
        return stockController.search(itemName, itemType);
    }

    /**
     * ------------------------------------------------------------------------
     * Run Client related code onwards.
     * ------------------------------------------------------------------------
     */
    private int addedItems;

    @Override
    public void run() {
        int requestCount = 0;
        final long startTime = System.currentTimeMillis();
        while ((requestCount++ < 100000) || (getElapsedTimeInSeconds(startTime) > 5)) {
            addedItems += userAdd(itemToBuy, 1);
        }
        System.out.println("Added items: " + addedItems);
    }

    private int getElapsedTimeInSeconds(long startTime) {
        return new Long((System.currentTimeMillis() - startTime) / MILLISECONDS_TO_SECONDS_RATIO).intValue();
    }

    public int getAddedItems() {
        return addedItems;
    }
}
