package facebook.businesslogic;

import facebook.model.Item;
import facebook.model.SearchResult;
import facebook.shared.OutOfStockException;

import java.io.*;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Stock implements Serializable {
    private static final long serialVersionUID = 1L;

    private static final String STOCK_DB_FILENAME = "stock.db";

    private static final int MAX_QUANTITY = 100000;
    private Map<Item, Integer> stockSupply = new ConcurrentHashMap<>();

    public String getStockList() {
        return stockSupply.toString();
    }

    public synchronized int addSupply(Item item, int quantity) {
        if (!stockSupply.containsKey(item)) {
            if (quantity > MAX_QUANTITY) {
                stockSupply.put(item, MAX_QUANTITY);
                return MAX_QUANTITY;
            }

            stockSupply.put(item, quantity);
            return quantity;
        }

        final int existingQuantity = stockSupply.get(item);
        int updatedQuantity = existingQuantity + quantity;
        if (updatedQuantity > MAX_QUANTITY) {
            updatedQuantity = MAX_QUANTITY;
        }

        stockSupply.put(item, updatedQuantity);
        return updatedQuantity - existingQuantity;
    }

    public int consumeSupply(Item item, int requestedQuantity) throws OutOfStockException {
        synchronized (this) {
            if (!stockSupply.containsKey(item) || stockSupply.get(item) == 0) {
                throw new OutOfStockException(item.getName() + " is out of stock!");
            }

            final int existingQuantity = stockSupply.get(item);
            int updatedQuantity = existingQuantity - requestedQuantity;
            if (updatedQuantity < 0) {
                stockSupply.put(item, 0);
                return existingQuantity;
            }

            stockSupply.put(item, updatedQuantity);
        }
        return requestedQuantity;
    }

    public void saveState() throws IOException {
        final File file = new File(getStoragePath());
        System.out.println(file.getCanonicalPath());
        try (FileOutputStream fileOutputStream = new FileOutputStream(file);
             BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(bufferedOutputStream)) {
            objectOutputStream.writeObject(this);
        }
    }

    public void loadState() throws IOException, ClassNotFoundException {
        try (FileInputStream fileInputStream = new FileInputStream(new File(getStoragePath()));
             BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
             ObjectInputStream objectInputStream = new ObjectInputStream(bufferedInputStream)) {
            final Stock loadedStock = (Stock) objectInputStream.readObject();
            stockSupply = loadedStock.stockSupply;
        }
    }

    public void saveStateNotBuffered() throws IOException {
        final ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream("stock.db"));
        outputStream.writeObject(this);
        outputStream.close();
    }

    public void loadStateNotBuffered() throws IOException, ClassNotFoundException {
        final ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream("stock.db"));
        Stock loadedStock = (Stock) inputStream.readObject();
        //stockSupply = loadedStock.stockSupply;
        stockSupply.clear();
        stockSupply.putAll(loadedStock.stockSupply);
        inputStream.close();
    }

    private String getStoragePath() {
        return getClass().getResource(".").getPath().split("/target/")[0] + "/target/" + STOCK_DB_FILENAME;
    }

    /**
     * Searches for item name of the given type in the stock.
     * <p>
     * Suppress warning as there is a filter before casting, so the type conforms.
     *
     * @param itemName the search item name
     * @param itemType the search item type
     * @param <T>      type of the item, for example: Laptop, Frigider
     * @return the list of items found.
     */
    @SuppressWarnings("unchecked")
    <T extends Item> List<SearchResult<T>> search(String itemName, Class<T> itemType) {
        // Parcurg toate elementele
        // Selectez elementele care au numele dat
        // Returnez elementele selectate
        Predicate<SearchResult<T>> filterCondition = searchResult ->
                searchResult.getItem().getName().toLowerCase()
                        .matches(".*" + itemName.toLowerCase() + ".*");

        return stockSupply.entrySet().stream()
                .filter(entry -> itemType.isInstance(entry.getKey()))
                .map(entry -> new SearchResult<>((T) entry.getKey(), entry.getValue()))
                .filter(filterCondition)
                .collect(Collectors.toList());
    }
}
