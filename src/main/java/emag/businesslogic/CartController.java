package facebook.businesslogic;

import facebook.model.Item;
import facebook.shared.OutOfStockException;

import java.util.Map;

public class CartController {
    private final Stock stock;
    private final Cart cart;

    public CartController(Stock stock, Cart cart) {
        this.stock = stock;
        this.cart = cart;
    }

    public int userAdd(Item item, int quantity) throws OutOfStockException {
        final int availableStock = stock.consumeSupply(item, quantity);
        final int addedInCart = cart.addCartItem(item, availableStock);
        return addedInCart;
    }

    public int userRemove(Item item, int quantity) {
        final int removedFromCart = cart.removeItem(item, quantity);
        final int returnedToStock = stock.addSupply(item, removedFromCart);
        return returnedToStock;
    }

    public void userRemoveAll() {
        final Map<Item, Integer> removedItems = cart.removeAll();
        for (Map.Entry<Item, Integer> entry : removedItems.entrySet()) {
            stock.addSupply(entry.getKey(), entry.getValue());
        }
    }
}
