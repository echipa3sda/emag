package facebook.shared;

public class OutOfStockException extends Exception {
    public OutOfStockException(String message) {
        super(message);
    }
}
