package facebook.model;

public class SearchResult<T extends Item> {
    private T item;
    private int quantity;

    public SearchResult(T item, int quantity) {
        this.item = item;
        this.quantity = quantity;
    }

    public T getItem() {
        return item;
    }

    public int getQuantity() {
        return quantity;
    }
}
