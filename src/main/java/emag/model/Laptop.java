package facebook.model;


import facebook.shared.Procesor;

public class Laptop extends Item {
    private int memory;
    private Procesor procesor;

    public Laptop(String name, int price, int memory, Procesor procesor) {
        super(name);
        this.price = price;
        this.memory = memory;
        this.procesor = procesor;
    }

    public int getMemory() {
        return memory;
    }

    public Procesor getProcesor() {
        return procesor;
    }
}
