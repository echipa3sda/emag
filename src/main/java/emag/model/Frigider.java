package facebook.model;

import facebook.shared.ConsumEnergie;

public class Frigider extends Item {
    private ConsumEnergie consumEnergie;
    private int volum;

    public Frigider(String name, int price, ConsumEnergie consumEnergie, int volum) {
        super(name);
        this.price = price;
        this.consumEnergie = consumEnergie;
        this.volum=volum;
    }

    public ConsumEnergie getConsumEnergie() {
        return consumEnergie;
    }

    public int getVolum() {
        return volum;
    }
}
