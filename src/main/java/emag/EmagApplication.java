package facebook;

import facebook.businesslogic.Cart;
import facebook.businesslogic.Client;
import facebook.businesslogic.Stock;
import facebook.model.Frigider;
import facebook.model.Item;
import facebook.model.Laptop;
import facebook.model.SearchResult;
import facebook.shared.ConsumEnergie;
import facebook.shared.OutOfStockException;
import facebook.shared.Procesor;

import java.io.IOException;
import java.util.List;

/**
 * Future features:
 * - typed stock: hash map with < Class, HashMap < Class, Integer > >, avoid casts and suppress warnings on search.
 * - subtotals printing
 */
public class EmagApplication {
    private static final String DASH = "-";
    private static final String SEPARATOR = " " + DASH + " ";
    private static final String SECTION_SEPARATOR = new String(new char[20]).replace("\0", DASH);

    public static void main(String[] args) throws InterruptedException {
        final Stock stock = new Stock();
        final Item laptopAcer = new Laptop("acer", 2000, 8, Procesor.INTEL);
        final Item laptopDell = new Laptop("dell laptop", 3000, 16, Procesor.AMD);
        final Item frigiderDell = new Frigider("dell fridge", 2000, ConsumEnergie.A, 100);
        final Item laptopHp = new Laptop("hp", 2900, 16, Procesor.INTEL);
        stock.addSupply(laptopAcer, 10);
        stock.addSupply(laptopDell, 100000);
        stock.addSupply(frigiderDell, 20000);

        searchProducts(stock, laptopAcer, laptopDell, frigiderDell);

        stockInteraction(stock, laptopAcer, laptopDell, laptopHp);

        cartInteraction(laptopAcer, laptopDell);

        clientInteractionMultiThreaded(stock, laptopDell);

        saveAndLoadState(stock, laptopAcer);
    }

    private static void searchProducts(Stock stock, Item laptopAcer, Item laptopDell, Item frigiderDell) {
        searchProducts(stock, laptopDell);
        searchProducts(stock, frigiderDell);
        searchProducts(stock, laptopAcer);
        System.out.println(SECTION_SEPARATOR);
    }

    private static <T extends Item> void searchProducts(Stock stock, T item) {
        Client client = new Client(stock, item);
        String searchName = item.getName().substring(2, 4);
        System.out.println("Search string: " + searchName);

        if (item instanceof Laptop) {
            List<SearchResult<Laptop>> searchResults = client.search(searchName, Laptop.class);
            searchResults.forEach(EmagApplication::processLaptopResults);
            System.out.println();
            return;
        }
        List<SearchResult<Item>> searchResults = client.search(searchName, Item.class);
        searchResults.forEach(EmagApplication::processItemResults);
        System.out.println();
    }

    private static void processItemResults(SearchResult<Item> searchResult) {
        Item laptopFound = searchResult.getItem();
        System.out.println(laptopFound.getName() + SEPARATOR +
                laptopFound.getPrice() + SEPARATOR +
                searchResult.getQuantity());
    }

    private static void processLaptopResults(SearchResult<Laptop> searchResult) {
        Laptop laptopFound = searchResult.getItem();
        System.out.println(laptopFound.getName() + SEPARATOR +
                laptopFound.getPrice() + SEPARATOR +
                laptopFound.getProcesor() + SEPARATOR +
                laptopFound.getMemory() + SEPARATOR +
                searchResult.getQuantity());
    }

    private static void stockInteraction(Stock stock, Item item1, Item item2, Item item3) {
        try {
            System.out.println("Stock interaction:");
            int rezervateItem1 = stock.consumeSupply(item1, 12);
            System.out.println("Item1: " + item1.getName() + SEPARATOR + rezervateItem1);
            int rezervateItem2 = stock.consumeSupply(item2, 9);
            System.out.println("Item2: " + item2.getName() + SEPARATOR + rezervateItem2);
            int rezervateItem3 = stock.consumeSupply(item3, 1);
            System.out.println("Item3: " + item3.getName() + SEPARATOR + rezervateItem3);
        } catch (OutOfStockException e) {
            System.out.println(e.getMessage());
        } finally {
            System.out.println(SECTION_SEPARATOR);
        }

    }

    private static void cartInteraction(Item item1, Item item2) {
        System.out.println("Cart interaction:");
        final Cart cart = new Cart();
        cart.addCartItem(item1, 10);
        cart.addCartItem(item2, 30);
        try {
            cart.printCheckoutToFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(cart.checkout());
        System.out.println(cart.checkout());
        System.out.println(SECTION_SEPARATOR);
    }

    private static void clientInteractionMultiThreaded(Stock stock, Item laptopDell) throws InterruptedException {
        System.out.println("Client interaction:");
        final Client client1 = new Client(stock, laptopDell);
        final Client client2 = new Client(stock, laptopDell);
        final Thread shoppingSimulation1 = new Thread(client1);
        final Thread shoppingSimulation2 = new Thread(client2);
        shoppingSimulation1.start();
        shoppingSimulation2.start();

        shoppingSimulation1.join();
        shoppingSimulation2.join();
        final int purchasedItemsTotal = client1.getAddedItems() + client2.getAddedItems();
        System.out.println("Total items purchased: " + purchasedItemsTotal + System.lineSeparator() + SECTION_SEPARATOR);
    }

    private static void saveAndLoadState(Stock stock, Item item) {
        try {
            System.out.println("State interaction: ");
            stock.addSupply(item, 10);
            stock.saveState();
            System.out.println("Saved state: " + stock.getStockList());
            stock.consumeSupply(item, 5);
            System.out.println("Changed state: " + stock.getStockList());
            stock.loadState();
            System.out.println("Restored state: " + stock.getStockList());
        } catch (ClassNotFoundException | IOException | OutOfStockException e) {
            e.printStackTrace();
        } finally {
            System.out.println(SECTION_SEPARATOR);
        }
    }
}
